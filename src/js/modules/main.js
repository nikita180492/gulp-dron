export const main =()=>{

  let x = [".svg"];
  x.forEach(item => {
    $(item).each(function () {
      let $img = $(this);
      let imgClass = $img.attr("class");
      let imgURL = $img.attr("src");
      $.get(imgURL, function (data) {
        let $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
          $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
          $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        $img.replaceWith($svg);
      }, "");
    });
  });


  var nav = $('nav');
  var line = $('<div />').addClass('line');

  line.appendTo(nav);

  var active = nav.find('.active');
  var pos = 0;
  var wid = 0;

  if (active.length) {
    pos = active.position().left;
    wid = active.width();
    line.css({
      left: pos,
      width: wid
    });
  }

  function find() {
    nav.find('ul li button').click(function (e) {
      e.preventDefault();
      if (!$(this).parent().hasClass('active') && !nav.hasClass('animate')) {

        nav.addClass('animate');

        var _this = $(this);

        nav.find('ul li').removeClass('active');

        var position = _this.parent().position();
        var width = _this.parent().width();

        if (position.left >= pos) {
          line.animate({
            width: ((position.left - pos) + width)
          }, 300, function () {
            line.animate({
              width: width,
              left: position.left
            }, 150, function () {
              nav.removeClass('animate');
            });
            _this.parent().addClass('active');
          });
        } else {
          line.animate({
            left: position.left,
            width: ((pos - position.left) + wid)
          }, 300, function () {
            line.animate({
              width: width
            }, 150, function () {
              nav.removeClass('animate');
            });
            _this.parent().addClass('active');
          });
        }

        pos = position.left;
        wid = width;
      }
    });
  }

  find()




  $("#sidebarCollapse").on("click", function () {

    $("#sidebar").toggleClass("active");
  });

  $("#dismiss, .overlay").on("click", function () {
    $("#sidebar").removeClass("active");
    $(".overlay").removeClass("active");
    $(".button-mobile-menu").find('#checkbox1').prop('checked', false);
  });

  $("#sidebarCollapse").on("click", function () {
    $("#sidebar").addClass("active");
    $(".overlay").addClass("active");
    $(".collapse.in").toggleClass("in");
    $("a[aria-expanded=true]").attr("aria-expanded", "false");
    $(".button-mobile-menu").find('#checkbox1').prop('checked', true);
  });

  $(".js-news-menu-button").on("click", function () {

    $(this).toggleClass("news-menu-button--open");
    $(".news-navigation-overlay").toggleClass("news-navigation-overlay--open");
    $(".shift").toggleClass("shift--active");
    let shiftArrowButton = $(".news-navigation-overlay").get(0).getBoundingClientRect().width - $(this).get(0).getBoundingClientRect().width - 9;
    console.log(shiftArrowButton);
    if ($(this).hasClass("news-menu-button--open")) {
      $(".new__image").css("max-height", "484px");
      $(this).css("transform", `rotate(180deg)`);
      $(this).css("left", `${shiftArrowButton}px`);
      //
      $("html, body").animate({ scrollTop: 0 }, "slow");
      // $("html, body").css('overflow', 'hidden');


    } else {
      $(".new__image").css("max-height", "584px");
      $(this).css("transform", "rotate(0deg)");
      $(this).css("left", "8px");
      // $("html, body").css('overflow', 'auto');
    }

  });

  //Дождёмся загрузки API и готовности DOM.
  ymaps.ready(init);
  let destinations = {
    'OR': [55.749633, 37.537434], //г. Москва, Пресненская наб., дом 12, Башня Федерации «Запад», 35 эт., пом.8
  };

  function init() {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    var myMap = new ymaps.Map('map', {
      // При инициализации карты обязательно нужно указать
      // её центр и коэффициент масштабирования.
      center: destinations['OR'], //
      zoom: 15
    });
    let myPlacemark = new ymaps.Placemark(destinations['OR'], { //
      // Хинт показывается при наведении мышкой на иконку метки.
      hintContent: 'Башня Федерации «Запад»',
      // Балун откроется при клике по метке.
      balloonContentBody:
        '<div class="tooltip-address-title">г. Москва, Пресненская наб., дом 12, Башня Федерации «Запад», 35 эт., пом.8</div>'
    }, {
      //опции
      iconLayout: 'default#image',
      // iconImageHref: 'img/map-point.png',
      iconImageSize: [43, 48],
      iconImageOffset: [-27, -54],
    });
    myMap.geoObjects.add(myPlacemark);
  }

  $('.slider-center').slick({
    centerMode: true,
    // centerPadding: '60px',
    // dots: true,
    slidesToShow: 3,
    rows: 0,
    arrows: true,
    // variableWidth: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          centerMode: true,
          // centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1000,
        settings: {
          centerMode: true,
          // centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          // centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          centerMode: true,
          // centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  })


}

