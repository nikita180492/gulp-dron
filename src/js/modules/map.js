import data from "../../files/data.json";

export const map = () => {

  const Declination = (number, titles) => {
    let cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
  }

  let title = $('#title-map')
  let desc_map = $('#desc-map')
  let players_map = $('#players-map')
  let players_text = $('#players-text')
  const drawText = () => {
    let glowInTexts = document.querySelectorAll(".glowIn");
    glowInTexts.forEach(glowInText => {
      let letters = glowInText.textContent.split("");
      // console.log(letters)
      glowInText.textContent = "";
      letters.forEach((letter, i) => {
        let span = document.createElement("span");
        span.textContent = letter;
        span.style.animationDelay = `${i * 0.02}s`;
        glowInText.append(span);
      });
    });
  }

  let selectLocation = ''

  drawText()
  $('.map-image svg').click(function (event) {
    let location = $(event.target).attr('id')
    $('#g7028 path').removeClass('has-info')
    let region = data.filter(item => item.code === location)[0]
    selectLocation = location
    $(`#${selectLocation}`).addClass('has-info')
    renderInfoMap(region)
    drawText()
  })

  $('body').on('click', '.drop-down-list-city__item', function(event){
    event.preventDefault()
    event.stopPropagation()
    $('.drop-down-list-city__item').removeClass('drop-down-list-city__item--active')
    $(this).addClass('drop-down-list-city__item--active')
    $('.drop-down-list-city-pc').removeClass('drop-down-list-city-pc--active')
    let location = $(event.target).attr('data-code')
    let region = data.filter(item => item.code === location)[0]
    selectLocation = location
    $('#g7028 path').removeClass('has-info')
    $(`#${selectLocation}`).addClass('has-info')
    renderInfoMap(region)
    drawText()
  })
    // .has-info
  let count = 1
  let items = ''
  let cols = ''
  data.forEach((item, i) => {
    items += `<div class="drop-down-list-city__item" data-code="${item.code}">${item.title}</div>`
    if (count < 6) {
      count++
    } else {
      cols += `<div class="drop-down-list-city__col">${items} </div>`
      count = 1
      items = ''
    }
  })

  if (count !== 1) {
    cols += `<div class="drop-down-list-city__col">${items}</div>`
  }
  $('.drop-down-list-city__items').html(cols)

  const renderInfoMap =(region)=>{
    $(title).text(region.title);
    $(desc_map).text(region.desc);
    $(desc_map).text(region.desc);
    $(players_map).text(region.players);
    $(players_text).text(Declination(region.players, ['участник', 'участника', 'участников']));
  }

  $('.js-title-map-icon').click(function (event) {
    event.preventDefault()
    event.stopPropagation()
    $('.drop-down-list-city-pc').toggleClass('drop-down-list-city-pc--active')

    $('.drop-down-list-city__item').removeClass('drop-down-list-city__item--active')
    $('.drop-down-list-city__item').each(function(i){
      if( $('.drop-down-list-city__item')[i].getAttribute('data-code') === selectLocation){
        $( this ).addClass('drop-down-list-city__item--active')
      }
    })
  })
}
