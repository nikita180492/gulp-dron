export const drag =()=>{
  const slider = document.querySelector('.drop-down-list-city__items');
  if(!slider) return
  let isDown = false;
  let startX;
  let scrollLeft;

  slider.addEventListener('mousedown', (e) => {
    isDown = true;
    slider.classList.add('drop-down-list-city__items--active');
    startX = e.pageX - slider.offsetLeft;
    scrollLeft = slider.scrollLeft;
  });
  slider.addEventListener('mousemove', (e) => {

    if(!isDown) return;
    e.preventDefault();
    const x = e.pageX - slider.offsetLeft;
    const walk = (x - startX) * 3; //scroll-fast
    slider.scrollLeft = scrollLeft - walk;

  });
  slider.addEventListener('mouseleave', (e) => {
    e.preventDefault();
    isDown = false;
    slider.classList.remove('drop-down-list-city__items--active');
  });
  slider.addEventListener('mouseup', (e) => {

    isDown = false;
    slider.classList.remove('drop-down-list-city__items--active');
  });

}
