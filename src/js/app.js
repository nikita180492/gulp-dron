// import './modules/jquery.js'
import $ from 'jquery'
import 'slick-carousel'
import * as flsFunctions from "./modules/functions.js";
import * as timer from './modules/timer.js'
import * as main from './modules/main.js'
import * as drag from './modules/drag.js'
import * as map from './modules/map.js'
import '../../node_modules/bootstrap/js/dist/tab.js'
import '../../node_modules/bootstrap/js/dist/collapse.js'
import { Fancybox } from "@fancyapps/ui/src/Fancybox/Fancybox.js";

window.jQuery = window.$ = $;
flsFunctions.isWebp();
timer.timer()
main.main()
map.map()
drag.drag()


// import Swiper, { Navigation, Pagination } from 'swiper';
// const swiper = new Swiper();

